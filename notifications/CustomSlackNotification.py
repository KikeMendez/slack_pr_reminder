
from slackclient import SlackClient

class CustomSlackNotification(object):
	"""
	Custom class to send slack notifications
	"""

	@classmethod
	def postReviewerMessage(cls, slack, channel,title, url, reviewer):

		slack.api_call(
			"chat.postMessage",
			channel="#{}".format(channel),
			text="\n *{}:*  {} \n *_`{}`_* \n\n".format(title, url, reviewer),
		)

	@classmethod
	def postNoReviewerMessage(cls, slack, channel, message, title, url):

		slack.api_call(
			"chat.postMessage",
			channel="#{}".format(channel),
			text="{} \n *{}:* \n {} \n\n".format(message, title, url),
		)