import os

from os.path import join, dirname

from dotenv import load_dotenv

from github import Github

from slackclient import SlackClient

from notifications.CustomSlackNotification import CustomSlackNotification as Notify

dotenv_path = join(dirname(__file__), '.env')
load_dotenv(dotenv_path)

# github access token
github = Github(os.getenv('GITHUB_TOKEN'))

# slack access token
slack = SlackClient(os.getenv('SLACK_TOKEN'))

# slack channel
channel = os.getenv('SLACK_CHANNEL')

results = []

for repository in github.get_user().get_repos():

	repository_name = repository.name

	# Get all open PR per repository
	for pull in repository.get_pulls('open'):

		pull_review_data = {}
		pull_title = pull.title
		pull_number = pull.number

		pull_url = "https://github.com/Firmstep/{}/pull/{}".format(repository_name, pull_number)

		pull_review_data['repository'] = repository_name
		pull_review_data['pull_title'] = pull_title
		pull_review_data['pull_number'] = pull_number
		pull_review_data['pull_url'] = pull_url

		# Get all pr reviewers
		reviews = pull.get_review_requests()

		for reviewers in reviews:

			reviewer_name = []

			for reviewer in reviewers:

				if reviewer.__class__.__name__ != 'Team': # get just users reviewers

					reviewer_name.append(reviewer.login)

					pull_review_data['reviewers'] = reviewer_name

		results.append(pull_review_data)

# Send Slack Notification
for result in results:

	if 'reviewers' in result.keys():

		Notify.postReviewerMessage(slack, channel, result['pull_title'], result['pull_url'], result['reviewers'])

	else:

		message = 'Pull request open but has not reviewers assigned:'

		Notify.postNoReviewerMessage(slack, channel, message, result['pull_title'], result['pull_url'])
